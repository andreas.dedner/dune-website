+++
title = "Dune Developer Meeting 2023 in Dresden"
+++

Date
----

The developer meeting is held on September 19th and 20th, 2023 at the Technische Universität Dresden.

The meeting is co-located with the [Dune User Meeting 2023](/community/meetings/2023-09-usermeeting).

Participants
-------------

- Simon Praetorius
- Oliver Sander
- Robert
- Carsten
- Christian Engwer

Video conference
----------------

The developer meeting will be streamed and allows online participation via BigBlueButton.

[Start BBB videoconference](https://tud.link/gza0)


Proposed topics
---------------

Please use the edit function to add your topics or write an email to [the organizers](mailto:meeting-2023@dune-project.org).

## General

* RK: Developers (old and new) and future management structure!
* RK: Stability of the gitlab infrastructure. How can we help to improve it?
* CGü: I'd like to see an hour of cleaning up. Together we scan (one or two of)
  - merge requests
  - bugs
  - the remaining open FlySpray tasks
  - Git branches

  and decide which can be closed / deleted right away and which to keep.

## dune-common

* OS: Simon and Santiago have invested a lot of time thinking about improvements to the build system.
  Can we decide on a way forward?  Can some of the work be merged?

* SO: MPIHelper [dune-common:1274](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/1274)
  - RK: What about MPI_Comm_dup while we are at it?

## dune-geometry

## dune-grid

* `VirtualGrid` (aka `PolymorphicGrid`) (roadmap?) [dune-grid:661](https://gitlab.dune-project.org/core/dune-grid/-/merge_requests/661)
* Can `PolymorphicGrid` simplify the Python bindings?

## dune-localfunctions

* RK: Shape function evaluations should be thread safe. [dune-localfunctions:22](https://gitlab.dune-project.org/core/dune-localfunctions/-/issues/22)
* SO: RT shape functions only for tets [dune-localfunctions:20](https://gitlab.dune-project.org/core/dune-localfunctions/-/issues/20)

## dune-istl

## Discretization modules (FEM, PDELab, ...)

## Python support

* OS: What is the current state of the Python bindings?
* OS: Can we get some/better documentation about how to write bindings for you own code?  (For example, dune-functions bases?)
* OS: Are Julia bindings conceivable?`

## etc

* RK: To twist or not to twist? I would like to hear opinions [dune-alugrid:89](https://gitlab.dune-project.org/extensions/dune-alugrid/-/merge_requests/89)

# Protocol

TODO
