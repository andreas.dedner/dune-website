+++
date = "2016-12-19"
title = "Dune 2.5.0 Released"
tags = [ "releases" ]
+++

<span style="font-variant: small-caps">Dune</span> 2.5.0 has been
released as an early Christmas gift.  You can [download the tarballs],
checkout the `v2.5.0` tag via Git, or get prebuilt packages from
Debian unstable.

Included in the release are the [core modules][] (dune-common,
dune-geometry, dune-grid, dune-grid-howto, dune-istl, dune-localfunctions)
and several modules from the [staging area][] (dune-functions, dune-typetree,
dune-uggrid).

Many deprecated features have been removed, including the Autotools build
system, dune-grid's `EntityPointer` and `SGrid`; some features have been
moved to new extension modules (for example dune-alugrid and dune-grape).
Dune now also requires a compiler supporting C++11 and parts of C++14 (as
supported by gcc 4.9).  Please refer to the [release notes] for a more
detailed listing of what has changed in the new release.

  [download the tarballs]: /releases/2.5.0
  [core modules]: https://gitlab.dune-project.org/core/
  [staging area]: https://gitlab.dune-project.org/staging/
  [release notes]: /releases/2.5.0#dune-2-5-release-notes
