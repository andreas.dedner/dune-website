+++
date = "2013-04-26"
title = "New UG Patch File Allows Dynamic Load-Balancing of Node Data"
+++

We have released a new version of the [UG patch file]($(ROOT)/external_libraries/install_ug.html). The new patch level number is "10". It brings many improvements for running UG on machines with larger processor numbers. Also, it allows to do dynamic load-balancing with node data in both 2d and 3d grids. Consult the [changelog]($(ROOT)/external_libraries/install_ug.html) for details.
