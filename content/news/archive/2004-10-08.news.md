+++
date = "2004-10-08"
title = "6th DUNE Developer Meeting in Freiburg (Breisgau)"
+++

Oktober 07. / 08. the 7th DUNE Developer Meeting took place. The main points we were discussing were

-   the parallel and adaptive Grid Interface enhancements,
-   the new matrix / vector classes ([dune/istl](https://gitlab.dune-project.org/core/dune-istl/)) introduced by Peter,
-   code cleanup and reorganisation.
-   the [meeting minutes](/community/meetings/2004-10-07-devmeeting).
