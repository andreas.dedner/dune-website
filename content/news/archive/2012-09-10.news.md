+++
date = "2012-09-10"
title = "Reminder: Freiburg Dune School, October 8-12 2012"
+++

University of Freiburg, Germany

Note: Registration deadline is Sunday, September 30<sup>th</sup> 2012.

This one week course held in Freiburg will give an introduction to the Dune core modules including the Dune grid interface library, and the [Dune-Fem module](http://dune.mathematik.uni-freiburg.de/index.html).

The course will focus on the numerical treatment of partial differential equations using continuous and discontinuous Galerkin methods. These schemes will be implemented with the Dune-Fem module. The final third part of the course will highlight on the discretization of PDEs on stationary and evolving surfaces.

Further information and a registration form can be found [here](https://dune.mathematik.uni-freiburg.de/schools/index.html).
