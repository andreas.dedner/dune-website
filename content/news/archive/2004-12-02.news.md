+++
date = "2004-12-02"
title = "OneDGrid Reaches Stable State"
+++

A new implementation of the grid interface has been added to DUNE. **OneDGrid** gives you nonuniform one-dimensional grids with local grid refinement and coarsening. It is completely contained in the DUNE repository and can be used without any additional libraries.
