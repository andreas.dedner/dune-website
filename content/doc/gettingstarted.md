+++
title = "Getting started"
toc = 1
+++

{{< rightpanel >}}
#### More info
... can be be found in the [Dune book](/doc/book/) by Oliver Sander.

<a href="/doc/book/">
<img src="/img/dune-book-cover.png" width="50%" />
</a>

The chapter on
*How to get started with the C++ code*
[can be read online](https://tu-dresden.de/mn/math/numerik/sander/ressourcen/dateien/sander-getting-started-with-dune-2-7.pdf).

Videos on different aspects of DUNE are available [online](https://dune-pdelab-course.readthedocs.io/).
{{< /rightpanel >}}

*You want to install and use Dune on your machine and you are a complete
novice? Then this is the right place to start!*

We will give a short introduction to Dune and guide you through the
installation process.

## What is Dune?
Dune is a software framework  for the numerical solution of partial
differential equations (PDEs) written in C++. This means that it provides a
set of classes that help you to write your own PDE solver. It is not
an application with a fancy graphical user interface that you can just
run, type in your PDE and look at the result. Instead you write a C++
program that includes various pieces from the framework and glues them
together to solve a particular PDE with a particular method. It is,
however, quite flexible in letting you implement various different
solution procedures. Dune does also provide Python bindings for a large
part of its core features which can help with the rapid prototyping of new
code or for pre- and postprocessing tasks.

## Installing Dune

It is important to note that installation of Dune requires a computer system
with a relatively recent operating system, either
Linux or Apple MacOS. Windows is not officially
supported, but installation can be done via virtualization, see [installation on Windows](/doc/installation/installation-windows-wsl).
There are a number of different ways how to install and use Dune on
your computer (*click on the links to follow the instructions*):

### Dune C++ binary packages

This is the most convenient way to get started if you do __not__ want to modify the Dune sources.

The details depend on the system you are using:

 * [Installation on Debian and Ubuntu systems](/doc/installation/installation-deb)
 * [Installation on Windows via WSL+Ubuntu](/doc/installation/installation-windows-wsl)

### Python packages via PyPI

[Installation of Dune from the Python Package Index (PyPi)](/doc/installation/installation-pip).
This is another very convenient way to install Dune which does not
require root access and works on a broad range of systems including
MacOS for which binary packages might not be available.

### Full installation from source

Sooner or later you will most likely want to use the latest Dune
features or even add new features to the Dune code modules. This will
require you to [build Dune from source](/doc/installation/installation-buildsrc).

 * For convenience Peter Bastian maintains a set of scripts that [automate the installation](/doc/installation/installation-helper-script) on Linux.

## Running a first example

### Your first example with C++

You can find a more detailed description of these first steps with
some background in the
[first chapter](https://tu-dresden.de/mn/math/numerik/sander/ressourcen/dateien/sander-getting-started-with-dune-2-7.pdf)
of Oliver Sander's [Dune book](/doc/book/).
It describes installation from binary packages,
installation from the source repository, how to create your own Dune module, and also how to
solve a simple PDE with the Dune core modules.

### Your first example with Python

You can test the installation
by constructing a simple structured grid. Call `python` and then execute
```
from dune.grid import structuredGrid
grid = structuredGrid([0,0],[1,1],[10,10])
print( grid.size(0) ) # should return 100
grid.plot() # requires the installation of `matplotlib`
```

For a more detailed description of the Python bindings take a look at the
section on the [DUNE grid interface](/sphinx/content/sphinx/dune-fem/dune-corepy_nb.html) as part of the [dune-fem tutorial](/sphinx/content/sphinx/dune-fem/).

## Working with further Dune modules

### [dune-PDELab](/modules/dune-pdelab)
[Instructions for installing from the source.](/doc/gettingstarted/beginners-resources-pdelab) and using the [tutorials](/modules/dune-pdelab-tutorials/).

Recordings of the virtual DUNE/PDELab course 2021 are available for self-study. The [material can be found here](https://dune-pdelab-course.readthedocs.io/).

### [dune-fem](/modules/dune-fem)
[Detailed instructions on how to install dune-fem  and its Python bindings using `pip`.](/doc/gettingstarted/beginners-resources-dunefem).

