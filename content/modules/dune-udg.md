+++
group = ["extension"]
module = "dune-udg"
requires = ["dune-common", "dune-grid", "dune-istl", "dune-tpmc", "dune-pdelab", "dune-functions"]
short = "Allows for an easy implementation of Unfitted Discontinuous Galerkin methods on cut-cell grids."
maintainers = "[Christian Engwer](mailto:christi@dune-project.org)"
title = "dune-udg"
+++

The _dune-udg_ module allows for an easy implementation of Unfitted Discontinuous
Galerkin methods on cut-cell grids.

It offers integration with [dune-pdelab](https://dune-project.org/modules/dune-pdelab/).

### Applications

<figure class="figure center-block img-thumbnail"
        style="margin-left:0px;margin-bottom:20px;padding:10px;width:300px;">
<img class="figure-img img-fluid img-rounded"
     src="/img/unfitted-dg-vector.png"
     style="margin-left:30px;margin-right:30px;width:220px;height:auto;text-align:center"></td>
<figcaption class="figure-caption text-xs-right">An unfitted dG cut-cell discretization of laminar flow at the pore-scale.</figcaption>
</figure>

It has been used to solve

* elliptic and parabolic PDEs using the UDG method
* elliptic PDEs using ghost-penalty stabilized CutFEM
* UDG for surface PDEs, using similar concepts as Trace-FEM
* linear hyperbolic conservation law using a stabilized Cut-DG method

### Download

* Version compatible with Dune 2.6 [[tar.gz](/download/dune-udg/dune-udg-2.6.tar.gz),[zip](/download/dune-udg/dune-udg-2.6.zip)]

### Publications and Documentation
The UDG method is published in

* {{% doi doi="10.1002/nme.2631" %}}
* Ch. Engwer, An Unfitted Discontinuous Galerkin Scheme for Micro-scale Simulations and Numerical Upscaling, Heidelberg University, 2009. ([PDF](http://www.ub.uni-heidelberg.de/archiv/9990))

An older implementation of _dune-udg_ is described in

* {{% doi doi="10.1007/978-3-642-28589-9_7" %}}

### Maintainers

dune-udg is developed and maintained by [the team](https://www.uni-muenster.de/AMM/engwer/) of [Christian Engwer](https://www.uni-muenster.de/AMM/engwer/team/engwer.shtml).
